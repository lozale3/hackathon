import {LitElement, html} from 'lit-element';

class HolaMundo extends LitElement{
    /* render muestra la plantilla del componente*/
    render(){
        return html`
            <div>Hola Mundo!</div>
        `;
    }
}

/* Se crea la etiqueta "hola-mundo" */
customElements.define('hola-mundo', HolaMundo)