import {LitElement, html} from 'lit-element';

class PersonaSearch extends LitElement{
    static get properties(){
        return {
            fname: {type: String}
        };
    }
    
    constructor(){
        super();
    }
    render(){
        return html`

        <link href="./src/styles/style-form.css" rel="stylesheet" type="text/css">
        <div class="form">
        <form>
                <input type="text" @input="${this.getName}">
                <button @click="${this.moreInfo}" class="btn btn-info col-5 offset-1"><strong>Más información</strong></button>
        </form>
        
        </div>
        `;
    }

    getName (e) {
        console.log("getName en PersonaSearch");
        this.fname = e.target.value;
    }

    moreInfo (e) {

        console.log("moreInfo en PersonaSearch");
        console.log("moreInfo de la persona "+this.fname);

        this.dispatchEvent(
            new CustomEvent("info-person", {
                detail: {
                    name:this.fname
                }
            })
        )

        e.preventDefault();

    }

}

customElements.define('persona-search', PersonaSearch)