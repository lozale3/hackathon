import {LitElement, html} from 'lit-element';

class PersonaLogin extends LitElement{

    static get properties (){

        return  {
            credentials: {type:Object},
            validation: {type:Boolean}
        };
    }

    constructor (){
        super();
        this.resetFormData();
       
    }

    render(){

        return html`
       
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <link href="./src/styles/style-form.css" rel="stylesheet" type="text/css">

        <div class="login-page" id="login-person">
            <div class="form">
            
            <div id="block">
            </div>

                <form>
                    <div class="form-group">
                        <input id="personUserName" @input="${this.getUsername}" type="text" placeHolder="Usuario" required/>

                    </div>

                    <div class="form-group">
                        <input id="personPass" type="password" @input="${this.getPassword}" placeHolder="Contraseña" required/>
                    </div>

                    <button @click="${this.validateCredentials}"><strong>Acceder</strong></button>
                    <p class="message">No registrado? <a href="#">Create una cuenta</a></p>
                


                </form>
            </div>
        </div>
        `;
    }

    resetFormData() {

        // Inicializar para cuando vengamos del botón + desde el principio y que no aparezca undefined
        console.log("resetFormData");

        this.credentials = {};
        this.credentials.username="";
        this.credentials.password="";
        this.validation = true;
    }

    getUsername (e) {
        console.log("getUsername persona-login");
        this.credentials.username = e.target.value;
        e.preventDefault();

    }

    getPassword (e) {
        console.log("getPassword persona-login");
        this.credentials.password= e.target.value;
        e.preventDefault();
    }

    validateCredentials (e) {

        // Llamada al servicio que me devuelve si la autenticación es correcta o no
        let validate = true;

        if (validate) {

            // Mostrar todo OK
            console.log ("Validación correcta");

            this.dispatchEvent(
                new CustomEvent("person-validate",{
                    detail: {
                        validation: true
                    }
                })
            );

        }else{
            
            console.log ("Validación incorrecta");
            this.validation = false;
            // Mostrar error


            this.shadowRoot.getElementById("block").innerHTML ="<div id=\"show-error\"><label>Credenciales incorrectas</label></div>";


        }
        
      

        //e.preventDefault();

    }

}

customElements.define('persona-login',PersonaLogin);