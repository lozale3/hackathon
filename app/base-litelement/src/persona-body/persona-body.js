import {LitElement, html} from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';
import '../persona-catalog/persona-catalog.js';
import '../persona-app/persona-app.js';
//import '../persona-look-person/persona-look-person';
//import '../enabled-persona-search/enabled-persona-search.js';
import '../persona-login/persona-login.js';
import '../persona-search/persona-search.js';
import '../heroe-form/heroe-form.js';

class PersonaBody extends LitElement{
    static get properties(){
        return {
            heroes: {type: Array},
            showPersonApp: {type: Boolean},
            searchPersonApp: {type: Boolean}
            
        };
    }

    constructor(){
        super()
        this.showPersonApp = false
        this.searchPersonApp = false

        // Inicializar array

        this.heroes = [
            {
                name: "Spiderman",
                films:"El poder de Electro",
                img:"./src/images/spider.jpg"
                
                
            },
            {
                name: "Superman"
                
            },
            {
                name: "Ironman"
                
            }
        ];

    }

    render(){
        return html`
        <link href="./src/styles/style-form.css" rel="stylesheet" type="text/css">

        <div id="personaLogin">
           <persona-login @person-validate="${this.showOptions}"></persona-login>
        </div>
       <div id="personCatalog" class="disableLogin">
        <div id="personCatalog">
            <persona-catalog 
                 @list-superheroes="${this.enabledListSuperhero}"
                @search-superheroes="${this.enabledSearchSuperhero}"
                class="col-2"></persona-catalog>
                
            <persona-look-person show-person='{"personaAppEnable": ${this.showPersonApp}}'></persona-look-person>

            <div id="heroeSearch" class="disableLogin">
                <persona-search @info-person="${this.infoPerson}"> </persona-search>
                <heroe-form id="heroeForm"></heroe-form>
               
            <div>
        </div>
        `;
    }

    infoPerson (e) {
        console.log("infoPerson en persona-body")
        console.log("infoPerson de:"+e.detail.name);

        let chosenHeroe = this.heroes.filter(
            heroe => heroe.name === e.detail.name
        );

        this.shadowRoot.getElementById("heroeForm").person = chosenHeroe[0];

    }

    enabledListSuperhero(e){
        console.log("enableListSuperhero en persona-body")
        this.showPersonApp = true
    }

    enabledSearchSuperhero(e){
        console.log("enabledFindSuperhero en persona-body")

        //this.searchPersonApp = true
        this.shadowRoot.getElementById("heroeSearch").classList.remove("disableLogin");

    }
    showOptions(e){
        console.log("showOptions en PersonaBody");

        if (e.detail.validation){
            console.log("Usuario logado");
            this.shadowRoot.getElementById("personaLogin").classList.add("disableLogin");
            this.shadowRoot.getElementById("personCatalog").classList.remove("disableLogin");
            this.shadowRoot.getElementById("personCatalog").classList.add("showCatalog");
        }
    }

    
}

customElements.define('persona-body', PersonaBody)