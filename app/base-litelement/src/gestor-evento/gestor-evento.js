import {LitElement, html} from 'lit-element';
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';

class GestorEvento extends LitElement{
    /* render muestra la plantilla del componente*/
    render(){
        return html`
            <h1>Gestor Evento</h1>
            <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
            <!--Para seleccionar el receptor hay que indicarle un id (como en selenium)-->
            <receptor-evento id="receiver"></receptor-evento>
        `;
    }

    processEvent(e){
        console.log("Capturando el evento del emisor")
        console.log(e.detail)
        /* course hace referencia al course definido en la función sendEvent de la clase EmisorEvento*/
        this.shadowRoot.getElementById("receiver").course = e.detail.course
        this.shadowRoot.getElementById("receiver").year = e.detail.year
    }
}

/* Se crea la etiqueta "hola-mundo" */
customElements.define('gestor-evento', GestorEvento)