import {LitElement, html, css} from 'lit-element';

class TestBootstrap extends LitElement{

        static get styles(){
            return css`
                .redbg{
                    background-color: red;
                }
                .greenbg{
                    background-color: green;
                }
                .bluebg{
                    background-color: blue;
                }
                .greybg{
                    background-color: grey;
                }
            `;
        }
    render(){
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <h3>Test bootstrap</h3>
            <div class = "row greybg">
                <div class="col-2 col-sm-6 offset-1 redbg">Col 1</div>
                <div class="col-3 col-sm-1 offset-2 greenbg">Col 2</div>
                <div class="col-4 col-sm-1 bluebg">Col 3</div>
        `;
    }

}

customElements.define('test-bootstrap', TestBootstrap)