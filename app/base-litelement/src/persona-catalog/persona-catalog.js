import {LitElement, html} from 'lit-element';

class PersonaCatalog extends LitElement{

    static get properties (){

        return  {
            
            showCatalog: {type:Boolean}
        };
    }
    
    render(){
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <aside>
                <section>
                <div class="row w-100 align-items-center" >
    			    <div class="col text-center" >
                        <button @click="${this.listSuperheroes}" type="button" class="btn btn-primary">Listar</button>
                        <button type="button" class="btn btn-primary" @click="${this.searchSuperHeroe}">Buscar</button>
                        <!--<button @click="${this.newPerson}" class="w-100 btn bg-success" style="font-size: 50px"><strong>+</strong></button>-->
                    </div>
                </div>
                </section>
            </aside>
        `;
    }



    listSuperheroes(e){
        console.log("listSuperheroes en persona-catalog")
        e.preventDefault()
        this.dispatchEvent(new CustomEvent("list-superheroes", {}))
        console.log("listSuperheroes en persona-catalog2")
    }

    searchSuperHeroe (e) {
        console.log("searchSuperHeroe en persona-catalog");
    }
}

/* Se crea la etiqueta "hola-mundo" */
customElements.define('persona-catalog', PersonaCatalog)