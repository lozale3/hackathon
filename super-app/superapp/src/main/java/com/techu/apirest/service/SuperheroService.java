package com.techu.apirest.service;

import com.techu.apirest.model.SuperheroModel;
import com.techu.apirest.repository.SuperheroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SuperheroService {
    /// metodos que vamos a invocar desde el controlador.

    @Autowired
    SuperheroRepository superheroRepository;

    // Read
    public List<SuperheroModel> findAll(){
        return superheroRepository.findAll();
    }

    // Read by Id
    public Optional<SuperheroModel> findById(String id){
        return superheroRepository.findById(id);
    }

    public boolean existsById(String id) {
        return superheroRepository.existsById(id);
    }

    // Create
    public SuperheroModel save(SuperheroModel newSuperhero){
        return superheroRepository.save(newSuperhero);
    }

    // Delete
    public boolean isDeleted(SuperheroModel deletedSuperhero){
        try {
            superheroRepository.delete(deletedSuperhero);
            return true;
        } catch (Exception e){
            return false;
        }
    }

}
