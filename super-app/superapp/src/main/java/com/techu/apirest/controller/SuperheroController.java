package com.techu.apirest.controller;

import com.techu.apirest.model.MessageModel;
import com.techu.apirest.model.SuperheroModel;
import com.techu.apirest.service.SuperheroService;
import org.apache.logging.log4j.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/the-avengers/v0")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST}) // cross origin calls accepted
public class SuperheroController {

    @Autowired
    SuperheroService superheroService;

    /*@GetMapping("/superheros")
    public List<SuperheroModel> getSuperheros(){
        return superheroService.findAll();
    }*/

    @GetMapping("/superheros")
    public ResponseEntity<List<SuperheroModel>> getSuperheros(){
        try {
            List<SuperheroModel> listSph = superheroService.findAll();
            if (!listSph.isEmpty()) {
                return new ResponseEntity<List<SuperheroModel>>(listSph, HttpStatus.OK);
            }
            return new ResponseEntity<List<SuperheroModel>>((List<SuperheroModel>) null, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<List<SuperheroModel>>((List<SuperheroModel>) null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/superheros/{id}")
    public ResponseEntity<SuperheroModel> getSuperheroById(@PathVariable String id) {
        try {
            if (superheroService.findById(id).get() != null) {
                return new ResponseEntity<SuperheroModel>(superheroService.findById(id).get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<SuperheroModel>((SuperheroModel) null, HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<SuperheroModel>((SuperheroModel) null, HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping("/superheros")
    public ResponseEntity<MessageModel> postSuperhero(@RequestBody SuperheroModel newSuperhero) {
        try {
            SuperheroModel newSph = superheroService.save(newSuperhero);
            if (newSph != null) {
                HttpHeaders responseHeaders = new HttpHeaders();
                URI location = URI.create("superheros/" + newSuperhero.getId());
                responseHeaders.setLocation(location);
                return new ResponseEntity<MessageModel>(new MessageModel("201","Superhero successfully created.","OK"), responseHeaders, HttpStatus.CREATED);
            } else {
                return new ResponseEntity<MessageModel>(new MessageModel("400","Unable to create your superhero.","NOK"), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<MessageModel>(new MessageModel("500","Unexpected error.","NOK"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/superheros/{id}")
    public void putSuperhero(@RequestBody SuperheroModel updatedSuperhero, @PathVariable String id){
        if (superheroService.existsById(id)) {
            superheroService.save(updatedSuperhero);
        }
    }

    @DeleteMapping("/superheros/{id}")
    public boolean deleteSuperhero(@PathVariable String id){
        if (superheroService.existsById(id)) {
            SuperheroModel superheroToDelete = superheroService.findById(id).get();
            superheroService.isDeleted(superheroToDelete);
            return true;
        } else
            return false;
    }

    @PatchMapping("/superheros/{id}")
    public SuperheroModel patchSuperhero(@PathVariable String id, @RequestBody SuperheroModel patchedSuperhero) {
        if(superheroService.findById(id).isPresent() == true) {
            SuperheroModel superheroToPatch = superheroService.findById(id).get();
            if(patchedSuperhero.getName() != null) {
                superheroToPatch.setName(patchedSuperhero.getName());
            }
            if (patchedSuperhero.getDescription() != null) {
                superheroToPatch.setDescription(patchedSuperhero.getDescription());
            }
            if (patchedSuperhero.getSuperpower() != null) {
                superheroToPatch.setSuperpower(patchedSuperhero.getSuperpower());
            }
            //if (patchedSuperhero.isCanFly() != false) {
                superheroToPatch.setCanFly(patchedSuperhero.isCanFly());
            //}
            if (patchedSuperhero.getAge() != null) {
                superheroToPatch.setAge(patchedSuperhero.getAge());
            }
            return superheroService.save(superheroToPatch);
        }
        return null;
    }

}
