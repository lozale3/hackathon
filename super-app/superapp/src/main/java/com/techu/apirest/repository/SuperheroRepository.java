package com.techu.apirest.repository;

import com.techu.apirest.model.SuperheroModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SuperheroRepository extends MongoRepository<SuperheroModel, String> {

}
