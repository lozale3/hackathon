package com.techu.apirest.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "superheros")
public class SuperheroModel {

    @Id
    @NotNull
    private String id;
    private String name;
    private String description;
    private String superpower;
    private boolean canFly;
    private Integer age;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSuperpower() {
        return superpower;
    }

    public void setSuperpower(String superpower) {
        this.superpower = superpower;
    }

    public boolean isCanFly() {
        return canFly;
    }

    public void setCanFly(boolean canFly) {
        this.canFly = canFly;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public SuperheroModel(@NotNull String id, String name, String description, String superpower, boolean canFly, Integer age) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.superpower = superpower;
        this.canFly = canFly;
        this.age = age;
    }
}
