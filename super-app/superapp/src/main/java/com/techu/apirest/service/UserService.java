package com.techu.apirest.service;

import com.techu.apirest.model.UserModel;
import com.techu.apirest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;


    //Read by Id
    public Optional<UserModel> findById(String id) {

        return userRepository.findById(id);
    }


    //Create
    public UserModel save(UserModel user) {

        return userRepository.save(user);
    }


    //Delete by ID
    public boolean deleteById(String id) {
        try {
            userRepository.deleteById(id);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
    //Existy by ID
    public boolean existById(String id) {

        return userRepository.existsById(id);
    }
}
