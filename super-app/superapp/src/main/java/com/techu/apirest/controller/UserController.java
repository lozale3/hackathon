package com.techu.apirest.controller;


import com.techu.apirest.model.MessageModel;
import com.techu.apirest.model.UserModel;
import com.techu.apirest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/the-avengers/v0")

public class UserController {

    @Autowired

    UserService userService;

    @PostMapping("/register")
    public String postRegister(@RequestBody UserModel newUser) {
        if (newUser.getUser() != null && newUser.getPwd() != null) {
            if (userService.findById(newUser.getUser()).isPresent() == true) {
                return "User already exists";
            }
            userService.save(newUser);
            return "OK";
        }
        return "Invalid User/pwd";
    }

    @PostMapping("/login")
    public ResponseEntity<MessageModel> postLogin(@RequestBody UserModel newUser) {
        if (userService.findById(newUser.getUser()).isPresent() == true) {
            UserModel userLogin = userService.findById(newUser.getUser()).get();
            if (newUser.getUser().equals(userLogin.getUser())) {
                if (newUser.getPwd().equals(userLogin.getPwd())) {
                    return new ResponseEntity<MessageModel>(new MessageModel("200","Logged in!","OK"), HttpStatus.OK);
                }
                return new ResponseEntity<MessageModel>(new MessageModel("403","Invalid credentials","NOK"), HttpStatus.FORBIDDEN);
            }
            return new ResponseEntity<MessageModel>(new MessageModel("403","Invalid credentials","NOK"), HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<MessageModel>(new MessageModel("401","User doesn't exist","NOK"), HttpStatus.UNAUTHORIZED);
    }
}


